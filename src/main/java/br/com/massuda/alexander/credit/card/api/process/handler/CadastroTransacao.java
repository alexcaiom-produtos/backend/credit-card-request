/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.process.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.credit.card.api.model.Solicitacao;
import br.com.massuda.alexander.credit.card.api.model.Transacao;
import br.com.massuda.alexander.credit.card.api.process.Cadastro;
import br.com.massuda.alexander.credit.card.api.repository.TransacaoRepository;

/**
 * @author Alex
 *
 */
@Service
public class CadastroTransacao implements Cadastro {
	
	Cadastro proximo;
	
	@Autowired
	TransacaoRepository repository;
	
	@Override
	public Solicitacao cadastrar(Solicitacao o) {
		Transacao transacaoBD = repository.save(o.getTransacao());
		o.setTransacao(transacaoBD);
		proximo.cadastrar(o);
		return o;
	}

	@Override
	public void setProximo(Cadastro processo) {
		this.proximo = processo;
	}
}
