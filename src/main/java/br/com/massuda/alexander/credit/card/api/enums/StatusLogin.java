package br.com.massuda.alexander.credit.card.api.enums;

/**
 * @author Alex
 *
 */
public enum StatusLogin {

	NORMAL, BLOQUEADO;
	
}
