/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.credit.card.api.model.Solicitacao;
import br.com.massuda.alexander.credit.card.api.model.StatusSolicitacao;
import br.com.massuda.alexander.credit.card.api.repository.SolicitacaoRepository;

/**
 * @author Alex
 *
 */
@Service
public class SolicitacaoService {
	
	@Autowired
	SolicitacaoRepository repository;

	public StatusSolicitacao pesquisar(String cpf) {
		Solicitacao solicitacao = repository.findByCpf(cpf);
		return Objects.nonNull(solicitacao) ? solicitacao.getStatus() : null;
	}
	
	

}
