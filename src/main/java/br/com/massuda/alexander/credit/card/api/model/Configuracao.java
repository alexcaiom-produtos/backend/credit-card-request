/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.massuda.alexander.credit.card.api.util.Constantes;

/**
 * @author Alex
 *
 */
@Entity
@Table(schema = Constantes.DB_SCHEMA, name = "TB_CONFIGURACAO")
public class Configuracao {

	@Id
	private Long id;
	private String nome;
	private Boolean ativa;
	private String valor;
	@OneToMany(fetch = FetchType.LAZY)
	private List<Canal> canais;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Boolean getAtiva() {
		return ativa;
	}
	public void setAtiva(Boolean ativa) {
		this.ativa = ativa;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public List<Canal> getCanais() {
		return canais;
	}
	public void setCanais(List<Canal> canais) {
		this.canais = canais;
	}
}
