/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.process;

import br.com.massuda.alexander.credit.card.api.model.Solicitacao;

/**
 * @author Alex
 *
 */
public interface Cadastro {

	void setProximo(Cadastro processo);
	Solicitacao cadastrar(Solicitacao o);

}
