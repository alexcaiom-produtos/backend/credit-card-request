/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import br.com.massuda.alexander.credit.card.api.model.Individuo;

/**
 * @author Alex
 *
 */
@Repository
public interface IndividuoRepository extends JpaRepository<Individuo, Long>, JpaSpecificationExecutor<Individuo> {

	Optional<Individuo> findByCpf(String cpf);
	
}
