/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.credit.card.api.model.Solicitacao;
import br.com.massuda.alexander.credit.card.api.process.handler.ValidacaoBureau;
import br.com.massuda.alexander.credit.card.api.process.handler.ValidacaoClienteJaCadastrado;
import br.com.massuda.alexander.credit.card.api.process.handler.ValidacaoEsteganografia;
import br.com.massuda.alexander.credit.card.api.process.handler.ValidacaoLiveness;

/**
 * @author Alex
 *
 */
@Service
public class ProcessoValidacaoCadastro  {
	
//	@Autowired
//	private ValidacaoEsteganografia esteganografia;
//	@Autowired
//	private ValidacaoBureau bureau;
//	@Autowired
//	private ValidacaoLiveness liveness;
//	@Autowired
//	private ValidacaoClienteJaCadastrado clienteCadastrado;

	public void validar(Solicitacao o) {
		definirCadeiaResponsabilidade();
//		esteganografia.validar(o);
	}

	private void definirCadeiaResponsabilidade() {
//		esteganografia.setProximo(bureau);
//		bureau.setProximo(liveness);
//		liveness.setProximo(clienteCadastrado);
	}
	
}
