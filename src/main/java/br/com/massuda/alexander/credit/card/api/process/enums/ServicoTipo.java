/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.process.enums;

/**
 * @author Alex
 *
 */
public enum ServicoTipo {

	CARTAO_CREDITO_REQUISICAO("CARTAO_CREDITO_REQUISICAO", 111),
	CARTAO_CREDITO_PROCESSOR("CARTAO_CREDITO_PROCESSOR", 112);

	private String nome;
	private Integer prefixoErro;
	
	private ServicoTipo(String nome, int prefixoErro) {
		this.nome = nome;
		this.prefixoErro = prefixoErro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getPrefixoErro() {
		return prefixoErro;
	}

	public void setPrefixoErro(int prefixoErro) {
		this.prefixoErro = prefixoErro;
	}
	
}
