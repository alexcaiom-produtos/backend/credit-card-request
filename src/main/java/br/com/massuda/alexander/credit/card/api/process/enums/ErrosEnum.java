/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.process.enums;

import org.springframework.http.HttpStatus;

import br.com.massuda.alexander.credit.card.api.enums.Evento;

/**
 * @author Alex
 *
 */
public enum ErrosEnum implements ErroContrato {
	
	INDIVIDUO_JA_CADASTRADO(
			1,
			"O individuo ja esta cadastrado.",
			Evento.CADASTRO,
			ServicoTipo.CARTAO_CREDITO_REQUISICAO,
			HttpStatus.BAD_REQUEST);
	
	private Integer codigo;
	private String descricao;
	private ServicoTipo servico;
	private HttpStatus status;
	private Evento evento;
	
	private ErrosEnum(Integer codigo, String descricao, Evento evento, ServicoTipo servico, HttpStatus status) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.servico = servico;
		this.status = status;
		this.evento = evento;
	}

	@Override
	public String getCodigo() {
		return servico.getPrefixoErro().toString()+codigo.toString();
	}

	@Override
	public String getMensagem() {
		return descricao;
	}
	@Override
	public HttpStatus getHttpStatus() {
		return status;
	}

	@Override
	public String getServico() {
		return servico.getNome();
	}

	@Override
	public String getEvento() {
		return evento.getEvento();
	}
	
	

}
