/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.process.handler.exceptions;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.massuda.alexander.credit.card.api.process.dto.Mensagem;

/**
 * @author Alex
 *
 */
@ControllerAdvice
public class ErroHandler {
	
	@ExceptionHandler(NegocioException.class)
	public Mensagem tratar(NegocioException e) {
		return new Mensagem(e.getCodigo().toString(), e.getMensagem(), e.getOrigem(), e.getEvento());
	}

}
