/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.process.handler.exceptions;

import org.springframework.http.HttpStatus;

import br.com.massuda.alexander.credit.card.api.process.enums.ErroContrato;

/**
 * @author Alex
 *
 */
public class NegocioException extends RuntimeException {

	private Integer codigo;
	private String mensagem;
	private String origem;
	private String evento;
	private HttpStatus status;
	
	public NegocioException(ErroContrato erro) {
		super(erro.getMensagem());
		this.codigo = Integer.parseInt(erro.getCodigo());
		this.mensagem = erro.getMensagem();
		this.origem = erro.getServico();
		this.evento = erro.getEvento();
		this.status = erro.getHttpStatus();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 7005402850754233780L;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getEvento() {
		return evento;
	}

	public void setEvento(String evento) {
		this.evento = evento;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

}
