/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.model;

/**
 * @author Alex
 *
 */
public enum StatusSolicitacao {

	PROCESSANDO,
	APROVADA,
	REPROVADA;
	
}
