/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.process.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.credit.card.api.model.Solicitacao;
import br.com.massuda.alexander.credit.card.api.model.Individuo;
import br.com.massuda.alexander.credit.card.api.process.Cadastro;
import br.com.massuda.alexander.credit.card.api.repository.IndividuoRepository;

/**
 * @author Alex
 *
 */
@Service
public class CadastroIndividuo implements Cadastro {
	
	Cadastro proximo;
	@Autowired
	IndividuoRepository repository;
	
	@Override
	public Solicitacao cadastrar(Solicitacao o) {
		Individuo individuoBD = repository.save(o.getIndividuo());
		o.setIndividuo(individuoBD);
		proximo.cadastrar(o);
		return o;
	}

	@Override
	public void setProximo(Cadastro processo) {
		this.proximo = processo;
	}

	
	
}
