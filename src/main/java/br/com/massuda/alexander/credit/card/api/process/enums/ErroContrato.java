/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.process.enums;

import org.springframework.http.HttpStatus;

/**
 * @author Alex
 *
 */
public interface ErroContrato {
	
	
	String getCodigo();
	String getMensagem();
	String getServico();
	String getEvento();
	HttpStatus getHttpStatus();

}
