package br.com.massuda.alexander.credit.card.api.process.handler;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.credit.card.api.enums.IndividuoStatus;
import br.com.massuda.alexander.credit.card.api.model.Individuo;
import br.com.massuda.alexander.credit.card.api.model.Solicitacao;
import br.com.massuda.alexander.credit.card.api.process.ProcessoValidacao;
import br.com.massuda.alexander.credit.card.api.process.enums.ErrosEnum;
import br.com.massuda.alexander.credit.card.api.process.handler.exceptions.NegocioException;
import br.com.massuda.alexander.credit.card.api.repository.IndividuoRepository;

@Service
public class ValidacaoClienteJaCadastrado implements ProcessoValidacao {

	private ProcessoValidacao proximo;
	@Autowired
	private IndividuoRepository repository;

	@Override
	public void setProximo(ProcessoValidacao processo) {
		this.proximo = processo;
	}

	@Override
	public void validar(Solicitacao o) {
		executar(o);
		this.proximo.validar(o);
	}

	private void executar(Solicitacao o) {
		Individuo individuo = pesquisar(o.getIndividuo().getCpf());
		if (Optional.ofNullable(individuo).isPresent()
				&& IndividuoStatus.AUTENTICAVEL.equals(individuo.getStatus())) {
			throw new NegocioException(ErrosEnum.INDIVIDUO_JA_CADASTRADO);
		}
		
	}

	public Individuo pesquisar(String cpf) {
		Optional<Individuo> individuo = repository.findByCpf(cpf);
		return individuo.orElse(null);
	}
}
