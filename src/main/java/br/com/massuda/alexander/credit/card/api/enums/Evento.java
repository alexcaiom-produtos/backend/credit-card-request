/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.enums;

/**
 * @author Alex
 *
 */
public enum Evento {
	
	CADASTRO("CADASTRO");
	
	private String evento;

	private Evento(String evento) {
		this.evento = evento;
	}

	public String getEvento() {
		return evento;
	}

	public void setEvento(String evento) {
		this.evento = evento;
	}

}
