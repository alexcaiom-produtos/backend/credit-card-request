/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.massuda.alexander.credit.card.api.model.Solicitacao;

/**
 * @author Alex
 *
 */
@Repository
public interface SolicitacaoRepository extends JpaRepository<Solicitacao, Long>, JpaSpecificationExecutor<Solicitacao> {
	
	@Query("SELECT s FROM Solicitacao s WHERE s.individuo.cpf = :cpf")
	Solicitacao findByCpf(/* @Param("cpf") */ String cpf);

}
