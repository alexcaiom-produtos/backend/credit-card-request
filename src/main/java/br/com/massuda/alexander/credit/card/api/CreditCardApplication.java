package br.com.massuda.alexander.credit.card.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

import br.com.massuda.alexander.spring.framework.infra.web.config.Configuracao;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@Import({Configuracao.class, br.com.massuda.alexander.spring.framework.infra.config.Configuracao.class})
public class CreditCardApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(CreditCardApplication.class, args);
	}
    
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CreditCardApplication.class);
    }

}
