/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import br.com.massuda.alexander.credit.card.api.util.Constantes;

/**
 * @author Alex
 *
 */
@Entity
@Table(schema = Constantes.DB_SCHEMA, name = "TB_CANAL")
public class Canal {

	@Id
	private Long id;
	private String codigo;
	private String descricao;
	private String produto;
	@ManyToMany
	@JoinTable(
			name = "TB_CONFIG_CANAL",
			joinColumns = {
					@JoinColumn(name="CANAL_ID")
			},
			inverseJoinColumns = {
					@JoinColumn(name="CONFIGURACAO_ID")
			}
			)
	private List<Configuracao> configuracoes;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getProduto() {
		return produto;
	}
	public void setProduto(String produto) {
		this.produto = produto;
	}
	public List<Configuracao> getConfiguracoes() {
		return configuracoes;
	}
	public void setConfiguracoes(List<Configuracao> configuracoes) {
		this.configuracoes = configuracoes;
	}
}
