package br.com.massuda.alexander.credit.card.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.credit.card.api.model.Solicitacao;
import br.com.massuda.alexander.credit.card.api.process.ProcessoCadastro;
import br.com.massuda.alexander.credit.card.api.process.ProcessoValidacaoCadastro;

@Service
public class CadastroService {

	@Autowired
	private ProcessoValidacaoCadastro validacoes;
	@Autowired
	private ProcessoCadastro processo;
	
	public void cadastrar(Solicitacao o) {
		validacoes.validar(o);
		processo.cadastrar(o);
	}
	
}