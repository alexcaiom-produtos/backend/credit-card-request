package br.com.massuda.alexander.credit.card.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.massuda.alexander.credit.card.api.model.Solicitacao;
import br.com.massuda.alexander.credit.card.api.model.StatusSolicitacao;
import br.com.massuda.alexander.credit.card.api.service.CartaoCreditoService;

@CrossOrigin
@RestController
@RequestMapping("/evaluate")
public class CreditCardController {
	
	@Autowired
	CartaoCreditoService service;
	
	@PostMapping
	public void incluir(@RequestBody Solicitacao o) {
		service.cadastrar(o);
	}
	
	
	@GetMapping
	public StatusSolicitacao verificar(String cpf) {
		return service.get(cpf);
	}
	
}
