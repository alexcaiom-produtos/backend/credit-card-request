package br.com.massuda.alexander.credit.card.api.process.handler;

import org.springframework.stereotype.Component;

import br.com.massuda.alexander.credit.card.api.model.Solicitacao;
import br.com.massuda.alexander.credit.card.api.process.ProcessoValidacao;

@Component
public class ValidacaoBureau implements ProcessoValidacao {

	private ProcessoValidacao proximo;

	@Override
	public void setProximo(ProcessoValidacao processo) {
		this.proximo = processo;

	}

	@Override
	public void validar(Solicitacao o) {
		// TODO Auto-generated method stub
		proximo.validar(o);
	}

}
