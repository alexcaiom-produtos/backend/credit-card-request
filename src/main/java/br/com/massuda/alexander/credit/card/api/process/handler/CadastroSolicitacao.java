/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.process.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.credit.card.api.model.Solicitacao;
import br.com.massuda.alexander.credit.card.api.process.Cadastro;
import br.com.massuda.alexander.credit.card.api.repository.SolicitacaoRepository;

/**
 * @author Alex
 *
 */
@Service
public class CadastroSolicitacao implements Cadastro {
	
	Cadastro proximo;
	@Autowired
	SolicitacaoRepository repository;
	
	@Override
	public Solicitacao cadastrar(Solicitacao o) {
		repository.save(o);
		proximo.cadastrar(o);
		return o;
	}

	@Override
	public void setProximo(Cadastro processo) {
		this.proximo = processo;
	}
	
}
