/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.credit.card.api.model.Solicitacao;
import br.com.massuda.alexander.credit.card.api.process.handler.CadastroIndividuo;
import br.com.massuda.alexander.credit.card.api.process.handler.CadastroSolicitacao;
import br.com.massuda.alexander.credit.card.api.process.handler.CadastroTransacao;

/**
 * @author Alex
 *
 */
@Service
public class ProcessoCadastro {

	@Autowired
	CadastroTransacao cadastroTransacao;
	@Autowired
	CadastroIndividuo cadastroIndividuo;
	@Autowired
	CadastroSolicitacao cadastroSolicitacao;
	
	public void cadastrar(Solicitacao o) {
		definirCadeiaResponsabilidades();
		cadastroTransacao.cadastrar(o);
	}

	private void definirCadeiaResponsabilidades() {
		cadastroTransacao.setProximo(cadastroIndividuo);
		cadastroIndividuo.setProximo(cadastroSolicitacao);
	}

}
