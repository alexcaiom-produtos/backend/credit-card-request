/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.credit.card.api.model.Solicitacao;
import br.com.massuda.alexander.credit.card.api.model.StatusSolicitacao;

/**
 * @author Alex
 *
 */
@Service
public class CartaoCreditoService {
	
	@Autowired
	private CadastroService cadastro;
	@Autowired
	private SolicitacaoService solicitacaoService;

	public void cadastrar(Solicitacao o) {
		cadastro.cadastrar(o);
	}

	public StatusSolicitacao get(String cpf) {
		return solicitacaoService.pesquisar(cpf);
	}
	
}
