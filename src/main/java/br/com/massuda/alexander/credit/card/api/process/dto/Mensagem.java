/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.process.dto;

import java.io.Serializable;
import java.time.Instant;

import br.com.massuda.alexander.credit.card.api.process.enums.ErroContrato;

/**
 * @author Alex
 *
 */
public class Mensagem implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2079144272147717598L;
	
	private String codigo;
	private String mensagem;
	private String informacoesExtras;
	private String horario = Instant.now().toString();
	
	public Mensagem() {
		// TODO Auto-generated constructor stub
	}
	
	public Mensagem(String codigo, String mensagem, String servico, String evento) {
		this.codigo = codigo;
		this.mensagem = mensagem;
		this.informacoesExtras = servico + " - " + evento;
	}
	
	public Mensagem(ErroContrato erro) {
		this.codigo = erro.getCodigo();
		this.mensagem = erro.getMensagem();
		this.informacoesExtras = erro.getServico() + " - " + erro.getEvento();
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public String getInformacoesExtras() {
		return informacoesExtras;
	}
	public void setInformacoesExtras(String informacoesExtras) {
		this.informacoesExtras = informacoesExtras;
	}
	public String getHorario() {
		return horario;
	}
	public void setHorario(String horario) {
		this.horario = horario;
	}

}
