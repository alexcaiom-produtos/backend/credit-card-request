/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.massuda.alexander.credit.card.api.enums.IndividuoStatus;
import br.com.massuda.alexander.credit.card.api.util.Constantes;

/**
 * @author Alex
 *
 */
@Entity
@Table(schema = Constantes.DB_SCHEMA, name = "TB_INDIVIDUO")
public class Individuo  {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private IndividuoStatus status = IndividuoStatus.PROCESSANDO;
	private String cpf;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public IndividuoStatus getStatus() {
		return status;
	}
	public void setStatus(IndividuoStatus status) {
		this.status = status;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
