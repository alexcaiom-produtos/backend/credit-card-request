package br.com.massuda.alexander.credit.card.api.process.handler;

import org.springframework.stereotype.Service;

import br.com.massuda.alexander.credit.card.api.model.Solicitacao;
import br.com.massuda.alexander.credit.card.api.process.ProcessoValidacao;

@Service
public class ValidacaoEsteganografia implements ProcessoValidacao {

	private ProcessoValidacao proximo;

	@Override
	public void setProximo(ProcessoValidacao processo) {
		this.proximo = processo;
	}

	@Override
	public void validar(Solicitacao o) {
		executar(o);
		this.proximo.validar(o);
	}

	private void executar(Solicitacao o) {
		// TODO Auto-generated method stub
		
	}
	
}
