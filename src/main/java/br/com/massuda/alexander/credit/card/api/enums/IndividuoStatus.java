package br.com.massuda.alexander.credit.card.api.enums;

public enum IndividuoStatus {

	AUTENTICAVEL,
	CONFLITO,
	ERRO,
	PROCESSANDO,
	INATIVO;
	
}
