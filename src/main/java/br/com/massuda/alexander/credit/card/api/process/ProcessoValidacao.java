/**
 * 
 */
package br.com.massuda.alexander.credit.card.api.process;

import br.com.massuda.alexander.credit.card.api.model.Solicitacao;

/**
 * @author Alex
 *
 */
public interface ProcessoValidacao {
	void setProximo(ProcessoValidacao processo);
	void validar(Solicitacao o);
}
